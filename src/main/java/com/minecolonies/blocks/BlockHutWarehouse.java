package com.minecolonies.blocks;

public class BlockHutWarehouse extends BlockHut
{
    protected BlockHutWarehouse()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutWarehouse";
    }
}